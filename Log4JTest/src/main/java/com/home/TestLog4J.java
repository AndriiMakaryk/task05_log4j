package com.home;


import org.apache.log4j.Logger;

public class TestLog4J {
    final static Logger logger = Logger.getLogger(TestLog4J.class);

    public static void main(String[] args) {
        logger.info("I'm working with log4j");
    }
}
